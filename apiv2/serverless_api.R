suppressMessages( library( plumber ) ) ; suppressMessages( library( dplyr ) ) ; suppressMessages( library( caret ) )

suppressMessages( library( aws.s3 ) ) ; suppressMessages( library( pryr ) )

r <- plumb( "plumber_api.R" )  # Where 'plumber.R' is the location of the file shown above
#r <- plumb( "~/repos/cxapi/api/plumber_api.R")
r$run( port = 8000 )
