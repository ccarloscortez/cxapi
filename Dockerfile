FROM ccortez/cxrockerapi
MAINTAINER Carlos Cortez <ccortez@aws.pe>


COPY /apiv2/* /usr/local/lib/R/site-library/plumber/

#RUN R -e "install.packages('deplyr')"
RUN R -e "if(!require('devtools')) install.packages('devtools')"
RUN R -e "install.packages('plumber')"



EXPOSE 8001
ENTRYPOINT ["R", "-e", "pr <- plumber::plumb(commandArgs()[4]); pr$run(host='0.0.0.0', port=8001)"]

CMD ["/usr/local/lib/R/site-library/plumber/plumber_api.R"]