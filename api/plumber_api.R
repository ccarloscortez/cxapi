# plumber.R

#* Training model
#* @get /train
function( date, id_app, id_client, id_user, id_training, training_dataset_url, dependent_variable_name, access_key_dest, region_dest, secret_key_dest ){
  library( aws.s3 )
  Sys.setenv("AWS_ACCESS_KEY_ID" = access_key_dest,
             "AWS_SECRET_ACCESS_KEY" = secret_key_dest,
             "AWS_DEFAULT_REGION" = "us-east-1"
             )
  
  # get s3 object to dataframe
#  training_dataset_csv <- 
#    aws.s3::get_object("trainings/input/vidasecurity_train_dataset.csv", bucket = "artucxstorage", check_region = F) %>%
#    rawToChar() %>%
#    readr::read_csv()
  setTimeLimit(50);
    training_dataset_csv <- 
      aws.s3::get_object(training_dataset_url, check_region = F) %>%
      rawToChar() %>%
      readr::read_csv()
  
  churn_model_training_called = Chrun_Model_Training( training_dataset_csv, dependent_variable_name )
  
  
  if( churn_model_training_called$training_indicator ){
    
    # save a single object to s3
    s3saveRDS(x = churn_model_training_called$objModel, bucket = "artucxstorage", object = "trainings/output/model.rds", check_region = F)
    
    #saveRDS( churn_model_training_called$objModel, 'https://s3.amazonaws.com/artucx/id_training/model.rds' )
  
    train_api_output = list( 'saved_model' = 'https://s3.amazonaws.com/artucxstorage/trainings/output/model.rds',
                             
                             'id_app' = as.character( id_app ), 'id_client' = as.character( id_client ), 'id_user' = as.character( id_user ),
                             
                             'confusion_matrix' = churn_model_training_called$confusion_matrix_list,
                             
                             'accuracy' = churn_model_training_called$accuracy, 'fscore' = churn_model_training_called$fscore
                             
                           )
    
  } else{
    
    train_api_output = churn_model_training_called$train_message
    
  }
  
  return( train_api_output )
  
}


#.... Training ...

Chrun_Model_Training = function( training_dataset_csv, dependent_variable_name ){
  
  tryCatch({
    
    training_df = training_dataset_csv ; dependent_var = as.character( dependent_variable_name )
    
    if( dependent_var %in% names( training_df ) ){
      
      training_df[[dependent_var]] = as.factor( training_df[[dependent_var]] )
      
      training_df = training_df %>% Filter( function(x)!all(is.na(x)), . )
      
      training_df = training_df[ complete.cases( training_df ) , ]
      
      objControl = trainControl( method = 'cv', number = 3, returnResamp = 'none', summaryFunction = twoClassSummary, classProbs = T )
      
      objModel = train( training_df[ , setdiff( names( training_df ), dependent_var ) ], training_df[ , dependent_var ],
                       
                        method = 'gbm', trControl = objControl, metric = "ROC", preProc = c("center", "scale") )
      
      confustion_matrix = confusionMatrix.train( objModel ) ; confusion_mat_train = confustion_matrix$table
      
      confusion_matrix_list = list( 'tp' = confusion_mat_train[ 1, 1 ], 'fp' = confusion_mat_train[ 1, 2 ], 'fn' = confusion_mat_train[ 2, 1 ], 'nn' = confusion_mat_train[ 2, 2 ] )  
      
      accuracy = as.character( sum( diag( confusion_mat_train ) ) )
      
      precision = confustion_matrix[1,1]/sum( confustion_matrix[1,1:2] )
      
      recall = confustion_matrix[1,1]/sum( confustion_matrix[1:2,1] )
      
      fscore = ( 2*precision*recall )/( precision + recall )
      
      return( list( training_indicator = 1, confusion_matrix_list = confusion_matrix_list, accuracy = accuracy, 'model' = objModel, fscore = fscore ) )
      
    } else{
      
      return( list( training_indicator = 0, train_message = 'Dependent Variable is absent in input csv' ) )
      
    }
    
  }, error = function( cond ){
    
    return( list( training_indicator = 0, train_message = 'CSV not found in training_dataset_url' ) )
    
  })
  
}




